export const environment = {
  production: true,
  project: "coach",
  workShifts: true,
  api_base_url: 'https://coach.geton-training.de/?rest_route=/app'
};
