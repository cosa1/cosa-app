export const environment = {
  production: true,
  project: "coachstaging",
  workShifts: true,
  api_base_url: 'https://coachstaging.geton-training.de/?rest_route=/app'
};
