export const environment = {
  production: true,
  project: "c4c",
  workShifts: true,
  api_base_url: 'https://care4care-trainings.de/?rest_route=/app'
};