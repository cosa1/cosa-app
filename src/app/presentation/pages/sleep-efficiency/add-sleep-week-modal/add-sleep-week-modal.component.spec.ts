import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddSleepWeekModalComponent } from './add-sleep-week-modal.component';

describe('AddSleepWeekModalComponent', () => {
  let component: AddSleepWeekModalComponent;
  let fixture: ComponentFixture<AddSleepWeekModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSleepWeekModalComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddSleepWeekModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
