import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-add-sleep-week-modal',
  templateUrl: './add-sleep-week-modal.component.html',
  styleUrls: ['./add-sleep-week-modal.component.scss'],
})
export class AddSleepWeekModalComponent implements OnInit {

  @ViewChild('content') pageContentElem: any;

  title = "Meine Woche";
  saveButton = true;

  constructor(private modalController: ModalController) { }

  ngOnInit() { }


  async hasEnteredScores(event: any) {
    this.title = event.target.value;
    if (this.title == '') {
      this.saveButton = false;
    } else {
      this.saveButton = true;
    }
  }

  async handleClickSave(): Promise<void> {
    await this.modalController.dismiss({
      date: new Date(),
      title: this.title,
    });
  }
}
