import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SleepEfficiencyPage } from './sleep-efficiency.page';

const routes: Routes = [
  {
    path: '',
    component: SleepEfficiencyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SleepEfficiencyPageRoutingModule {}