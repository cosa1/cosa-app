import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SleepEfficiencyPageRoutingModule } from './sleep-efficiency-routing.module';
import { SleepEfficiencyPage } from './sleep-efficiency.page';
import { TitlebarModule } from '@app/presentation/universal-components/titlebar/titlebar.module';
import { SleepChartComponent } from './sleep-chart/sleep-chart.component';
import { TitlebarComponent } from '@app/presentation/universal-components/titlebar/titlebar.component';
import { LoadingSpinnerComponent } from '@app/presentation/universal-components/loading-spinner/loading-spinner.component';
import { LoadingSpinnerModule } from '@app/presentation/universal-components/loading-spinner/loading-spinner.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SleepEfficiencyPageRoutingModule,
    TitlebarModule,
    LoadingSpinnerModule,
  ],
  declarations: [
    SleepEfficiencyPage, SleepChartComponent,
  ],
  providers: [
    TitlebarComponent, LoadingSpinnerComponent,
  ],
})
export class SleepEfficiencyPageModule { }
