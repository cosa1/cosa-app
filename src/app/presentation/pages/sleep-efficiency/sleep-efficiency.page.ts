import { Component, OnInit, AfterViewInit, OnChanges } from '@angular/core';
import { Storage } from '@capacitor/core';
import { ModalController } from '@ionic/angular';
import { SleepService } from '@app/applicationlogic/sleep/sleep.service';
import { SleepEntry } from '@app/data/models/sleep-entry';
import { SleepEfficiencyStorageService } from '@app/data/sleep/sleep-efficiency-storage.service';
import { AddSleepWeekModalComponent } from './add-sleep-week-modal/add-sleep-week-modal.component';
import { SleepWeekEntry } from '@app/data/models/sleep-week-entry';

import { Router } from '@angular/router';
import { TitlebarComponent } from '@app/presentation/universal-components/titlebar/titlebar.component';
import { OfflineAlertService } from '@app/applicationlogic/alert/offline-alert.service';



@Component({
  selector: 'app-sleep-efficiency',
  templateUrl: './sleep-efficiency.page.html',
  styleUrls: ['./sleep-efficiency.page.scss'],
})
export class SleepEfficiencyPage implements OnInit {

  sleepEntries: SleepEntry[] = [];
  newEntry: SleepEntry;
  allWeeks: SleepWeekEntry[] = [];
  newWeekEntry: SleepWeekEntry;
  showAlert = false;
  canAddWeek = false;
  helpIsShown = false;
  sleepWeek: SleepWeekEntry;
  amountDays: number;
  showSpinner = false;

  constructor(
    private modalController: ModalController,
    private sleepService: SleepService, private sleepEfficiencyStorageService: SleepEfficiencyStorageService,
    private router: Router,
    private titlebarComponent: TitlebarComponent,
    private offlineAlertService: OfflineAlertService
  ) {
  }

  async ngOnInit() {
  }

  async ionViewWillEnter() {
    await this.getAllWeeks();
    await this.countAllDays();
    this.showAlert = await this.offlineAlertService.loadingAlert();
  }

  async getAllWeeks() {
    await this.sleepService.getAllWeeks().then(entries => {
      this.allWeeks = entries;
      this.canAddWeek = true;
    });
  }

  async countAllDays() {
    this.amountDays = 0;
    for (let i = 0; i < this.allWeeks.length; i++) {
      if (this.allWeeks[i].days.length > 0) {
        this.amountDays++
      }
    }
  }

  async handleClickAddWeek() {
    const modal = await this.modalController.create({
      component: AddSleepWeekModalComponent,
      cssClass: 'addSleepWeekModal'
    });
    await this.getAllWeeks();
    await modal.present();
    await this.getAllWeeks();


    await modal.onDidDismiss().then(event => {
      this.showSpinner = true;

      if (event.data) {
        this.newWeekEntry = {
          date: event.data.date,
          title: event.data.title,
          days: [],
        };
        this.sleepService.addWeek(this.newWeekEntry).then(() => {
          this.sleepService.getAllWeeks().then(entries => {
            this.allWeeks = entries;
            this.canAddWeek = true;
            this.offlineAlertService.loadingAlert().then(data => {
              this.showAlert = data;
            })

          })
        }).then(() => {
          this.showSpinner = false;

        });
      } else {
        this.showSpinner = false;

      }
    })
  }

  async handleClickOnEntry(entry: SleepWeekEntry) {
    const entries = await this.sleepEfficiencyStorageService.loadWeeksFromStorage();
    const indexLocal = entries.findIndex(e => e.id === entry.id);

    this.router.navigateByUrl('/review-sleep-week/' + indexLocal);
  }

  async handleClickDelete(entry: SleepWeekEntry) {
    this.showSpinner = true;
    //Visual delete
    const i = this.allWeeks.indexOf(entry);
    //delete from Local Storage and DB
    await this.sleepService.deleteWeek(entry, i).then(() => {
      this.allWeeks.splice(i, 1);
    });
    await this.countAllDays();
    this.showAlert = await this.offlineAlertService.loadingAlert();
    this.showSpinner = false;
  }

  toggleHelp() {
    this.helpIsShown = !this.helpIsShown;
  }
}