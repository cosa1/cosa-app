import { Component, OnInit, ViewChild, OnChanges, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-add-sleep-entry-modal',
  templateUrl: './add-sleep-entry-modal.component.html',
  styleUrls: ['./add-sleep-entry-modal.component.scss'],
})
export class AddSleepEntryModalComponent implements OnInit {

  @ViewChild('content') pageContentElem: any;

  date: Date;
  toBed: any;
  today = new Date();
  toBedDefault = (new Date()).toISOString();
  asleepDefault = new Date(this.today.getTime() + 600000).toISOString();
  wokeUpDefault = new Date(this.today.getTime() + (3600000 * 7) + 3000000).toISOString();
  getUpDefault = new Date(this.today.getTime() + (3600000 * 8)).toISOString();
  asleep: any;
  wokeUp: any;
  getUp: any;
  text: string;
  saveButton = false;
  cardIsShown = true;
  inputIsShown = true;
  protocolIsShown = false;
  selectedValue = "";
  errorMsg = false;
  yesNoQuestions = "Waren Sie nachts wach?";
  app_sleep_data_questions: any;
  morningCard = true;
  allEntry;
  entry;
  morningProtocol;
  eveningProtocol;
  protocol;
  title = "Neuer Eintrag";
  workShifts: boolean;


  constructor(private modalController: ModalController) { }

  ngOnInit() {
    this.workShifts = environment.workShifts
    this.app_sleep_data_questions = [];
    if (this.allEntry != undefined) {
      this.app_sleep_data_questions = this.allEntry.app_sleep_data_questions;
      this.selectedValue = this.allEntry.shift
      this.toBedDefault = this.allEntry.toBed
      this.asleepDefault = this.allEntry.asleep
      this.wokeUpDefault = this.allEntry.wokeUp
      this.getUpDefault = this.allEntry.getUp
      this.text = this.allEntry.text
      this.title = "Eintrag bearbeiten"
    }

    if (this.entry == undefined) {
      this.entry = [];
    }    

    if(this.workShifts){
      this.selectedValue = "Früh";
      }
    this.disableSaveBtn();
  }

  changeCardSlider() {
    this.morningCard = !this.morningCard;
  }

  getSaveMorningProtocol(app_sleep_data_questions, i) {
    this.app_sleep_data_questions[i] = app_sleep_data_questions;
  }

  async shiftValueChanged(event: any) {
    this.selectedValue = event.target.value;
  }

  toggleCard(card: string) {
    if (card == "cardIsShown") {
      this.cardIsShown = !this.cardIsShown;
    }
    else if (card == "inputIsShown") {
      this.inputIsShown = !this.inputIsShown;

    }
    else if (card == "protocolIsShown") {
      this.protocolIsShown = !this.protocolIsShown;
      this.cardIsShown = false;
      this.inputIsShown = false;
    }
  }

  async hasEnteredScores(toChangeName: string, event: any) {

    if (toChangeName === 'toBed') {
      this.toBedDefault = event.target.value;
      this.asleepDefault = new Date((new Date(event.target.value)).getTime() + 600000).toISOString()
      this.wokeUpDefault = new Date((new Date(event.target.value)).getTime() + (3600000 * 7) + 3000000).toISOString() 
      this.getUpDefault = new Date((new Date(event.target.value)).getTime() + (3600000 * 8)).toISOString() 
    }
    if (toChangeName === 'asleep') {
      this.asleepDefault = event.target.value;
    }
    if (toChangeName === 'wokeUp') {
      this.wokeUpDefault = event.target.value;
      this.getUpDefault = new Date((new Date(event.target.value)).getTime() + 600000).toISOString() 
    }
    if (toChangeName === 'getUp') {
      this.getUpDefault = event.target.value;
    }

    await this.disableSaveBtn();

  }

  async disableSaveBtn() {
    if (!!this.toBedDefault && !!this.asleepDefault && !!this.wokeUpDefault && !!this.getUpDefault) {
      if (await this.getMinutes(this.asleepDefault) < await this.getMinutes(this.wokeUpDefault) && await this.getMinutes(this.toBedDefault) <= await this.getMinutes(this.asleepDefault) && await this.getMinutes(this.wokeUpDefault) <= await this.getMinutes(this.getUpDefault)) {
        this.saveButton = true;
        this.errorMsg = false;
      }
      else {
        this.saveButton = false;
        this.errorMsg = true;
      }
    }
  }

  async handleClickSave(): Promise<void> {
    await this.modalController.dismiss({
      date: this.date,
      toBed: this.toBedDefault,
      asleep: this.asleepDefault,
      wokeUp: this.wokeUpDefault,
      getUp: this.getUpDefault,
      text: this.text,
      efficiency: await this.showSleepEfficiency(this.getUpDefault, this.toBedDefault, this.wokeUpDefault, this.asleepDefault),
      shift: this.selectedValue,
      app_sleep_data_questions: this.app_sleep_data_questions,
    });
  }

  async getMinutes(anyDate: any,) {
    const temp = (new Date(anyDate).getTime() / 60000).toString().split('.');
    return temp[0]
  }

  async showSleepEfficiency(getUp: any, toBed: any, wokeUp: any, asleep: any) {
    getUp = (new Date(getUp).getTime() / 60000).toString().split('.');
    toBed = (new Date(toBed).getTime() / 60000).toString().split('.');
    wokeUp = (new Date(wokeUp).getTime() / 60000).toString().split('.');
    asleep = (new Date(asleep).getTime() / 60000).toString().split('.');

    let bettliegezeit: number;
    let schlafzeit: number;
    if (getUp[0] == toBed[0]) {
      bettliegezeit = 1;
    }
    if (asleep[0] == wokeUp[0]) {
      schlafzeit = 1;
    } else {
      bettliegezeit = parseInt(getUp) - parseInt(toBed);
      schlafzeit = parseInt(wokeUp) - parseInt(asleep);
    }
    return Math.round(((schlafzeit) / (bettliegezeit)) * 100);
  }
}