import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Chart, registerables } from 'chart.js';
import { SleepWeekEntry } from '@app/data/models/sleep-week-entry';
import { SleepEntry } from '@app/data/models/sleep-entry';

@Component({
  selector: 'app-sleep-chart',
  templateUrl: './sleep-chart.component.html',
  styleUrls: ['./sleep-chart.component.scss'],
})
export class SleepChartComponent implements OnInit {
  @ViewChild('lineCanvas') private lineCanvas: ElementRef;

  @Input() sleepWeekEntries: SleepWeekEntry[];
  @Input() title: string;
  @Input() amount: any;
  @Input() type: string;
  @Input() shift = false;


  newAmount = 7;
  lineChart: any;
  selectedTitle: String;
  theType: string;
  myIndex = 0;
  theShift: string;
  shiftArr = ["Früh", "Spät", "Nacht", "Frei"];
  allDaysSorted = [];
  allDays = [];
  dataArr = [];
  labelArr = [];
  shiftEntries: SleepEntry[];

  constructor() { }

  ngOnInit() {
    this.selectedTitle = this.sleepWeekEntries[0].title;
    this.theType = this.type;
    this.theShift = "Früh";
  }

  async sortDays() {
    this.allDaysSorted = [];
    var sortedInMs = [];
    this.dataArr = [];
    this.labelArr = [];
    this.allDays = this.shiftEntries;
    for (let i = 0; i < this.allDays.length; i++) {
      sortedInMs.push(new Date(this.allDays[i].toBed).getTime());
    }
    while (this.allDays.length > 0) {
      await this.indexOfMax(sortedInMs).then(index => {
        this.allDaysSorted.push(this.allDays[index])
        sortedInMs.splice(index, 1);
        this.allDays.splice(index, 1);
      });
    }
    for (let i = 0; i < this.allDaysSorted.length; i++) {
      this.dataArr.push(this.allDaysSorted[i].efficiency);
      this.labelArr.push(new Date(this.allDaysSorted[i].toBed).getDate() + '.' + (new Date(this.allDaysSorted[i].toBed).getMonth() + 1) + '-' + new Date(this.allDaysSorted[i].getUp).getDate() + '.' + (new Date(this.allDaysSorted[i].getUp).getMonth() + 1))
    }
    return this.dataArr
  }

  async indexOfMax(arr: any) {
    var max = arr[0];
    var maxIndex = 0;
    if (arr.length === 0) {
      return -1;
    }
    for (var i = 1; i < arr.length; i++) {
      if (arr[i] > max) {
        maxIndex = i;
        max = arr[i];
      }
    }
    return maxIndex;
  }

  ngAfterViewInit() {
    this.lineChartMethod(this.myIndex);
  }

  async valueChanged(event: any) {
    await this.getWeekIdWithTitle(event.target.value).then(data => {
      this.myIndex = data;
    });
    await this.lineChart.destroy();
    this.lineChartMethod(this.myIndex);

  }

  async shiftChanged(event: any) {
    this.theShift = event.target.value;
    await this.lineChart.destroy();
    await this.lineChartMethod(this.myIndex);
  }

  async getWeekIdWithTitle(title: any) {
    for (let i = 0; i < this.sleepWeekEntries.length; i++) {
      if (this.sleepWeekEntries[i].title == title) {
        return i;
      }
    }
  }

  async lineChartMethod(myIndex: any) {
    const labels = [];
    const data = [];
    this.newAmount = 7;

    if (this.type == 'weeks') {
      if (this.amount > this.sleepWeekEntries.length) {
        this.newAmount = this.sleepWeekEntries.length
      } else {
        this.newAmount = this.amount
      }
      for (let i = 0; i < this.newAmount; i++) {
        if (this.sleepWeekEntries[i].efficiency != undefined && this.sleepWeekEntries[i].efficiency.toString() != "") {
          labels[this.newAmount - i - 1] = (new Date(this.sleepWeekEntries[i].days[0].toBed).getDate() + '.' + (new Date(this.sleepWeekEntries[i].days[0].toBed).getMonth() + 1) + '-' + new Date(this.sleepWeekEntries[i].days[this.sleepWeekEntries[i].days.length - 1].getUp).getDate() + '.' + (new Date(this.sleepWeekEntries[i].days[this.sleepWeekEntries[i].days.length - 1].getUp).getMonth() + 1));
          data[this.newAmount - i - 1] = (this.sleepWeekEntries[i].efficiency)
        } else {
          //code
        }
      }
    }

    else if (this.theType == 'days') {
      if (this.shift) {
        this.shiftEntries = [];

        if (this.amount > this.sleepWeekEntries[myIndex].days.length) {
          this.newAmount = this.sleepWeekEntries[myIndex].days.length
        } else {
          this.newAmount = this.amount
        }
        for (let j = 0; j < this.sleepWeekEntries.length; j++) {

          if (this.sleepWeekEntries[j].efficiency != undefined) {
            if (this.amount > this.sleepWeekEntries[j].days.length) {
              this.newAmount = this.sleepWeekEntries[j].days.length
            } else {
              this.newAmount = 7;
            }
            for (let i = 0; i < this.sleepWeekEntries[j].days.length; i++) {
              if (this.sleepWeekEntries[j].days[i].shift == this.theShift) {
                this.shiftEntries.push(this.sleepWeekEntries[j].days[i]);
              }
            }
          }
        }
        await this.sortDays().then(result => {
          let arrayLen;
          if (this.dataArr.length > 6) {
            arrayLen = 6;
          } else {
            arrayLen = this.dataArr.length;
          }
          for (let i = arrayLen - 1; 0 <= i; i--) {
            data.push(this.dataArr[i])
            labels.push(this.labelArr[i])
          }
        })
      } else {
        if (this.amount > this.sleepWeekEntries[myIndex].days.length) {
          this.newAmount = this.sleepWeekEntries[myIndex].days.length
        } else {
          this.newAmount = this.amount
        }
        for (let i = 0; i < this.newAmount; i++) {
          labels[this.newAmount - i - 1] = (new Date(this.sleepWeekEntries[myIndex].days[i].toBed).getDate() + '.' + (new Date(this.sleepWeekEntries[myIndex].days[i].toBed).getMonth() + 1) + '-' + new Date(this.sleepWeekEntries[myIndex].days[i].getUp).getDate() + '.' + (new Date(this.sleepWeekEntries[myIndex].days[i].getUp).getMonth() + 1))

          data[this.newAmount - i - 1] = ((this.sleepWeekEntries[myIndex].days[i]).efficiency)
        }
      }
    }
    else if (this.theType == 'months') {
      //TODO
    }
    else if (this.theType == 'years') {
      //TODO
    }
    Chart.register(...registerables);
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'line',
      options: {
        plugins: {
          legend: {
            display: false
          },
        },
        scales: {
          y: {
            suggestedMin: 0,
            max: 100,
            ticks: {
              stepSize: 10,

            }
          }
        }
      },
      data: {
        labels,
        datasets: [
          {
            fill: true,
            backgroundColor: 'rgba(0,0,0,0)',
            //backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 3,
            pointHitRadius: 10,
            data,
            spanGaps: false,
          }
        ]
      }
    });
  }
}
