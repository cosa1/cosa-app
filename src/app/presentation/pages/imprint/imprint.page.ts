import { Component, OnInit } from '@angular/core';
import { environment } from '@environment/environment';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-impressum',
  templateUrl: './imprint.page.html',
  styleUrls: ['./imprint.page.scss'],
})
export class ImprintPage implements OnInit {

  imprint_c4c = false;
  imprint_geton = false;

  constructor(private modalController: ModalController) {
  }

  ngOnInit() {
    if(environment.production == true && environment.project == "c4c"){
      this.imprint_c4c = true;
    }else{
      this.imprint_geton = true;
    }
  }
}
