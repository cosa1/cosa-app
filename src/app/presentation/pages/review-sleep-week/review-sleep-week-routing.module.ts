import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReviewSleepWeekPage } from './review-sleep-week.page';

const routes: Routes = [
  {
    path: '',
    component: ReviewSleepWeekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReviewSleepWeekPageRoutingModule {}
