import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReviewSleepWeekPageRoutingModule } from './review-sleep-week-routing.module';

import { ReviewSleepWeekPage } from './review-sleep-week.page';
import { SleepEfficiencyPageRoutingModule } from '../sleep-efficiency/sleep-efficiency-routing.module';
import { TitlebarModule } from '@app/presentation/universal-components/titlebar/titlebar.module';
import { AddSleepEntryModalComponent } from '../sleep-efficiency/add-sleep-entry-modal/add-sleep-entry-modal.component';
import { AddSleepWeekModalComponent } from '../sleep-efficiency/add-sleep-week-modal/add-sleep-week-modal.component';
import { TitlebarComponent } from '@app/presentation/universal-components/titlebar/titlebar.component';
import { LoadingSpinnerComponent } from '@app/presentation/universal-components/loading-spinner/loading-spinner.component';
import { LoadingSpinnerModule } from '@app/presentation/universal-components/loading-spinner/loading-spinner.module';
import { SliderComponent } from '@app/presentation/universal-components/slider/slider.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReviewSleepWeekPageRoutingModule,
    SleepEfficiencyPageRoutingModule,
    TitlebarModule,
    LoadingSpinnerModule,
  ],
  declarations: [ReviewSleepWeekPage, AddSleepEntryModalComponent, SliderComponent,
    AddSleepWeekModalComponent],
  providers: [
    TitlebarComponent, LoadingSpinnerComponent
  ],
})
export class ReviewSleepWeekPageModule { }
