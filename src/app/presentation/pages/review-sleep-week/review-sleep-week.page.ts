import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SleepEntry } from '@app/data/models/sleep-entry';
import { Storage } from '@capacitor/core';
import { SleepEfficiencyService } from '@app/data/sleep/sleep-efficiency.service';
import { SleepService } from '@app/applicationlogic/sleep/sleep.service';
import { SleepEfficiencyStorageService } from '@app/data/sleep/sleep-efficiency-storage.service';
import { AddSleepEntryModalComponent } from '../sleep-efficiency/add-sleep-entry-modal/add-sleep-entry-modal.component';
import { ActivatedRoute } from '@angular/router';
import { SleepWeekEntry } from '@app/data/models/sleep-week-entry';
import { TitlebarComponent } from '@app/presentation/universal-components/titlebar/titlebar.component';
import { OfflineAlertService } from '@app/applicationlogic/alert/offline-alert.service';



@Component({
  selector: 'app-review-sleep-week',
  templateUrl: './review-sleep-week.page.html',
  styleUrls: ['./review-sleep-week.page.scss'],
})
export class ReviewSleepWeekPage implements OnInit {

  cardIsShown = [];
  allWeeks: SleepWeekEntry[] = []
  sleepWeek: SleepWeekEntry;
  newEntry: SleepEntry;
  urlWeekId: number;
  index: number;
  maxSevenDaysInWeek = false;
  allDays = [];
  allDaysSorted = [];
  weekEfficiency: any;
  title: String;
  showAlert = false;
  weekIndex: number;
  showSpinner = false;
  morningCard = true;


  morningProtocol: { type: string, label: string, label2?: string, rangeLeft?: string, rangeRight?: string, rangeStepsArray?: any, newQuestion?: boolean, page: number, completed: boolean }[] = [
    { "type": "start", "label": "Morgen-Protokoll", "newQuestion": true, "page": 0, "completed": false },

    { "type": "yesNo", "label": "Waren Sie nachts wach?", "newQuestion": true, "page": 1, "completed": false },
    { "type": "inputNumber", "label": "Wie häufig sind Sie aufgewacht?", "label2": "Mal", "page": 2, "completed": false },
    { "type": "inputNumber", "label": "Wie lange waren Sie insgesamt wach?", "label2": "Minuten", "page": 3, "completed": false },

    { "type": "range", "label": "Wie erholsam fanden Sie ihren Schlaf?", "rangeStepsArray": ['Gar nicht erholsam', "Nicht erholsam", "Mittelmäßig", "Erholsam", "Sehr erholsam"], "newQuestion": true, "page": 4, "completed": false },

    { "type": "yesNo", "label": "Haben Sie Schlafmittel eingenommen?", "newQuestion": true, "page": 5, "completed": false },
    { "type": "inputString", "label": "Welche haben Sie eingenommen?", "page": 6, "completed": false },

    { "type": "yesNo", "label": " Wurden Sie während des Schlafens gestört?", "newQuestion": true, "page": 7, "completed": false },
    { "type": "inputString", "label": "Welche Störungen waren dies?", "page": 8, "completed": false },

    { "type": "end", "label": "Toll! Sie haben Ihr Morgen-Protokoll ausgefüllt.", "newQuestion": true, "page": 9, "completed": false },
  ];


  eveningProtocol: { type: string, label: string, label2?: string, rangeLeft?: string, rangeRight?: string, rangeStepsArray?: any, newQuestion?: boolean, page: number, completed: boolean }[] = [
    { "type": "start", "label": "Abend-Protokoll", "newQuestion": true, "page": 0, "completed": false },

    { "type": "range", "label": "Wie fühlen Sie sich heute?", "rangeStepsArray": ['Sehr angespannt', "Angespannt", "Mittelmäßig", "Entspannt", "Sehr entspannt"], "newQuestion": true, "page": 1, "completed": false },

    { "type": "range", "label": "Wie leistungsfähig waren Sie heute?", "rangeStepsArray": ['Gar nicht', "Wenig", "Mittelmäßig", "Etwas", "Sehr"], "newQuestion": true, "page": 2, "completed": false },

    { "type": "range", "label": "Wie erschöpft waren Sie heute?", "rangeStepsArray": ['Gar nicht', "Wenig", "Mittelmäßig", "Etwas", "Sehr"], "newQuestion": true, "page": 3, "completed": false },

    { "type": "yesNo", "label": "Haben Sie tagüber geschlafen?", "newQuestion": true, "page": 4, "completed": false },
    { "type": "inputNumber", "label": "Für wie lange?", "page": 5, "completed": false },

    { "type": "yesNo", "label": "Haben Sie koffeinhaltige Getränke getrunken?", "newQuestion": true, "page": 6, "completed": false },
    { "type": "inputString", "label": "Welche?", "page": 7, "completed": false },
    { "type": "inputString", "label": "Wie viel?", "page": 8, "completed": false },

    { "type": "yesNo", "label": "Haben Sie alkoholische Getränke getrunken?", "newQuestion": true, "page": 9, "completed": false },
    { "type": "inputString", "label": "Welche?", "page": 10, "completed": false },
    { "type": "inputString", "label": "Wie viel?", "page": 11, "completed": false },

    { "type": "end", "label": "Toll! Sie haben Ihr Abend-Protokoll ausgefüllt.", "newQuestion": true, "page": 12, "completed": false },
  ];

  protocol = [this.morningProtocol, this.eveningProtocol]


  constructor(
    private modalController: ModalController,
    private route: ActivatedRoute,
    private sleepService: SleepService,
    private offlineAlertService: OfflineAlertService

  ) {
  }

  async ngOnInit() {
    await this.initCardToggle();
    await this.getWeeks();
    await this.calcWeekEfficiency();
    await this.sortDays();
    this.weekIndex = this.allWeeks.findIndex(e => e.id === this.sleepWeek.id);
    this.showAlert = await this.offlineAlertService.loadingAlert();
  }

  async sortDays() {
    this.allDaysSorted = [];
    var sortedInMs = [];
    let indexToUpdate;

    for (let i = 0; i < this.allDays.length; i++) {
      sortedInMs.push(new Date(this.allDays[i].toBed).getTime());
    }
    while (this.allDays.length > 0) {
      await this.indexOfMax(sortedInMs).then(index => {
        this.allDaysSorted.push(this.allDays[index])
        sortedInMs.splice(index, 1);
        this.allDays.splice(index, 1);
        indexToUpdate = index;
      });
    }
    this.allDays = this.allDaysSorted;
    this.sleepWeek.days = this.allDays;
  }

  async indexOfMax(arr: any) {
    var max = arr[0];
    var maxIndex = 0;
    if (arr.length === 0) {
      return -1;
    }
    for (var i = 1; i < arr.length; i++) {
      if (arr[i] > max) {
        maxIndex = i;
        max = arr[i];
      }
    }
    return maxIndex;
  }


  async initCardToggle() {
    for (let i = 0; i < 7; i++) {
      let data = {
        show: false
      }
      this.cardIsShown.push(data);
    }
  }

  async getWeeks() {
    await this.getWeekId();
    await this.sleepService.getAllWeeks().then(entries => {
      this.allWeeks = entries;
      this.sleepWeek = entries[this.urlWeekId];
      this.title = this.sleepWeek.title;
      this.allDays = this.sleepWeek.days;
      this.disableAddButton()
    });
  }

  async calcWeekEfficiency() {
    this.weekEfficiency = 0;
    let counter = 0;
    for (let i = 0; i < this.allDays.length; i++) {
      if (this.allDays[i].efficiency != null) {
        counter++;
        this.weekEfficiency = this.weekEfficiency + this.allDays[i].efficiency;
      }
    }
    this.weekEfficiency = Math.round(this.weekEfficiency / counter);

    if (isNaN(this.weekEfficiency)) {
      this.weekEfficiency = "";
    }

    this.sleepWeek.efficiency = this.weekEfficiency;
  }

  async getWeekId() {
    //get week from url
    //id: index of week in local storage
    this.route.params.subscribe(params => {
      this.urlWeekId = Number(params['id']);
    });
  }

  async disableAddButton() {
    if ((this.sleepWeek.days).length <= 6) {
      this.maxSevenDaysInWeek = false;
    } else {
      this.maxSevenDaysInWeek = true;
    }
  }

  async toggleCard(index: number) {
    for (let i = 0; i < 7; i++)
      //toggle show
      if (index == i) {
        this.cardIsShown[i].show = !this.cardIsShown[i].show;
      }
      //toggle close
      else if (this.cardIsShown[i].show == true) {
        this.cardIsShown[i].show = !this.cardIsShown[i].show;
      }
  }


  async handleClickAddEntry(): Promise<void> {
    const modal = await this.modalController.create({
      component: AddSleepEntryModalComponent,
      cssClass: 'addSleepEntryModalComponent',
      componentProps: {
        protocol: this.protocol,
      }
    });
    await modal.present();
    await modal.onDidDismiss().then(event => {
      if (event.data) {
        this.newEntry = {
          date: event.data.toBed,
          toBed: event.data.toBed,
          asleep: event.data.asleep,
          wokeUp: event.data.wokeUp,
          getUp: event.data.getUp,
          text: event.data.text,
          efficiency: event.data.efficiency,
          shift: event.data.shift,
          app_sleep_data_questions: event.data.app_sleep_data_questions,
          sleepValue: event.data.sleepValue,

        };
        this.sleepWeek.days.unshift(this.newEntry);
        this.calcWeekEfficiency();
        this.sortDays().then(ev=>{
           this.sleepService.updateWeek(this.sleepWeek, this.allWeeks, this.urlWeekId);
        });

       

        (async () => {
          this.showSpinner = true;
          await this.delay(1500);
          this.showAlert = await this.offlineAlertService.loadingAlert();
          this.showSpinner = false;
        })();

        this.disableAddButton();

      }
    })
  }

  async handleClickDelete(entry: SleepEntry) {
    const i = this.allDays.indexOf(entry);
    this.allDays.splice(i, 1);
    this.sleepWeek.days = this.allDays;
    this.calcWeekEfficiency();
    this.sortDays();
    await this.sleepService.updateWeek(this.sleepWeek, this.allWeeks, this.urlWeekId);

    (async () => {
      this.showSpinner = true;
      await this.delay(1500);
      this.showAlert = await this.offlineAlertService.loadingAlert();
      this.showSpinner = false;
    })();

    await this.disableAddButton();

  }

  async handleClickEdit(entry: SleepEntry, i: number) {
    const modal = await this.modalController.create({
      component: AddSleepEntryModalComponent,
      cssClass: 'addSleepEntryModalComponent',
      componentProps: {
        allEntry: entry,
        entry: entry.app_sleep_data_questions,
        protocol: this.protocol,
      }
    });
    await modal.present();
    await modal.onDidDismiss().then(event => {
      if (event.data) {
        this.newEntry = {
          date: event.data.toBed,
          toBed: event.data.toBed,
          asleep: event.data.asleep,
          wokeUp: event.data.wokeUp,
          getUp: event.data.getUp,
          text: event.data.text,
          efficiency: event.data.efficiency,
          //weekId: this.urlWeekId,
          shift: event.data.shift,
          app_sleep_data_questions: event.data.app_sleep_data_questions,
          sleepValue: event.data.sleepValue
        };
        this.sleepWeek.days[i] = this.newEntry;
        this.calcWeekEfficiency();
        this.sortDays();
        this.sleepService.updateWeek(this.sleepWeek, this.allWeeks, this.urlWeekId);

        (async () => {
          this.showSpinner = true;
          await this.delay(1500);
          this.showAlert = await this.offlineAlertService.loadingAlert();
          this.showSpinner = false;
        })();

        this.disableAddButton();

      }
    })
  }

  changeCardSlider() {
    this.morningCard = !this.morningCard;
  }





  async delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}


