import { Component, OnInit } from '@angular/core';
import { SyncService } from '@app/applicationlogic/sync/services/sync.service';
import { AuthenticationService } from '@app/applicationlogic/auth/services/authentication.service';
import { Router } from '@angular/router';
import { OfflineAlertService } from '@app/applicationlogic/alert/offline-alert.service';


@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.page.html',
  styleUrls: ['./my-account.page.scss'],
})
export class MyAccountPage implements OnInit {

  currentUser$ = this.auth.getCurrentUser();
  lastSync$ = this.syncService.getLastSyncTimestamp();
  syncIsInProgress: boolean;
  showAlert: boolean;

  constructor(private syncService: SyncService, private auth: AuthenticationService, private router: Router,
    private offlineAlertService: OfflineAlertService) {
  }

  ngOnInit() {
    this.offlineAlertService.loadingAlert().then(data => {
      this.showAlert = data;
    })
  }

  handleClickLogout() {
    this.auth.logout().then(r => {
      window.location.reload();
    });
  }

  async handleClickSync() {
    this.syncIsInProgress = true;
    await this.syncService.fullSync().then(r => {
      this.syncIsInProgress = false;
      this.offlineAlertService.loadingAlert().then(data => {
        this.showAlert = data;
      })
    });
  }
}
