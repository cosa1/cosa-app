import { Component, OnInit } from '@angular/core';
import { UserTrainingService } from '@app/applicationlogic/training/services/user-training.service';
import { Training } from '@app/data/models/training';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { OfflineAlertService } from '@app/applicationlogic/alert/offline-alert.service';

@Component({
  selector: 'app-select-training',
  templateUrl: './select-training.page.html',
  styleUrls: ['./select-training.page.scss'],
})
export class SelectTrainingPage implements OnInit {

  shownTrainings: Training[];
  selectedTraining$ = this.userTrainingService.getSelectedTraining();
  showAlert = false;

  constructor(private userTrainingService: UserTrainingService, private router: Router, private toastController: ToastController, private offlineAlertService: OfflineAlertService) {
  }

  ngOnInit() {
    this.userTrainingService.getUserTrainings().then(trainings => {
      this.shownTrainings = trainings;
    });
    this.offlineAlertService.loadingAlert().then((data) => {
      this.showAlert = data;
    });
  }

  public trainingClickHandler(training: Training): void {
    this.userTrainingService.selectTraining(training);
    this.router.navigateByUrl('/home');
    this.toastController.create({ message: `${training.name} ausgewählt.`, duration: 2000 }).then(toast => {
      toast.present();
    });
  }

  doRefresh(event: Event | any) {
    this.userTrainingService.getUserTrainings().then(trainings => {
      this.shownTrainings = trainings;
      event.target.complete();
    });
  }
}
