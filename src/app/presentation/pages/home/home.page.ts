import { Component, OnInit } from '@angular/core';
import { OfflineAlertService } from '@app/applicationlogic/alert/offline-alert.service';
import { AuthenticationService } from '@app/applicationlogic/auth/services/authentication.service';
import { UserTrainingService } from '@app/applicationlogic/training/services/user-training.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  currentUser$ = this.auth.getCurrentUser();
  currentTraining$ = this.trainingService.getSelectedTraining();
  showAlert = false;
  containsSleepCare = false;
  containsSelfcompassion = false;
  constructor(
    private auth: AuthenticationService,
    private trainingService: UserTrainingService,
    private offlineAlertService: OfflineAlertService
  ) {
  }

  ngOnInit() {
    this.currentUser$ = this.auth.getCurrentUser();
    this.currentTraining$ = this.trainingService.getSelectedTraining();
    this.trainingService.getSelectedTraining().subscribe(entries => {
      if (entries.name.includes("SleepCare")) {
        this.containsSleepCare = true;
      }
      if (entries.name.includes("Nama") || entries.name.includes("NΛMΛH")) {
        this.containsSelfcompassion = true;
      }
    });
  }

  async ionViewWillEnter() {
    this.showAlert = await this.offlineAlertService.loadingAlert();
  }
}
