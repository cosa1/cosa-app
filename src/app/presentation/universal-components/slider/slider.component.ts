import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent implements OnInit {

  @ViewChild(IonSlides) slides: IonSlides;

  @Output() morningProtocolEvent = new EventEmitter<any[]>();
  @Input() protocol: any;
  @Input() entry: any;
  @Input() mode = "";
  @Input() key = "";
  @Output() app_sleep_data_questions: any[];

  pageAmountArray: number[];
  sliderrange;
  editMode;
  isLastPage = true;
  isFirstPage = false;
  inputNumber;
  inputString;
  currentSlide: any;
  slidesLength: any;

  constructor() { }
  ngOnInit() {
    this.app_sleep_data_questions = []

    this.sliderrange = 2;
    this.checkMode();

    if (this.entry == undefined) {
      this.entry = []
    }
    this.setDefaultValues()
  }

  async setDefaultValues(){
    //Range default value
    for(let i = 0; i < this.protocol.length; i++){
      if (this.protocol[i].type == "range"){
        if(this.entry[this.protocol[i].page] == null){
          this.entry[this.protocol[i].page] = 2;
        }
      }
    }
  }

  async checkMode() {
    if (this.entry == undefined) {
      this.editMode = false;
    }
    else if (this.entry != undefined) {
      this.editMode = true;
      this.app_sleep_data_questions = this.entry;
    }

  }

  async toSave(page, text, type?) {
    this.app_sleep_data_questions[page] = text;
    this.morningProtocolEvent.emit(this.app_sleep_data_questions);
  }

  async slideChanged() {
    if (this.mode != 'readOnly') {
      let me = this;
      this.slides.isEnd().then((istrue) => {
        if (istrue) {
          this.slides.length().then(data => {
            // set if protocol is completed
            this.app_sleep_data_questions[data - 1] = "completed"
          })
          this.morningProtocolEvent.emit(this.app_sleep_data_questions);
        }
      });
    }

    await this.slides.getActiveIndex().then(index => {
      this.currentSlide = index + 1;
    })

  }

  slideOpt = {
    //direction: 'horizontal',
    slidesPerView: 1,
    allowTouchMove: false,
    ionSlideNextEnd: 1,
  }

  async nextPage(slides, element) {
    slides.slideNext(); // slide to next
    if (element.type == "range" && (this.app_sleep_data_questions[element.page] == undefined || this.app_sleep_data_questions[element.page] == null)) {
      this.app_sleep_data_questions[element.page] = 2;
      this.morningProtocolEvent.emit(this.app_sleep_data_questions);
    }
  }

  async nextQuestion(slides, i: number) {
    for (i = i + 1; i < (await slides.length()); i++) {
      if (this.protocol[i].newQuestion == true) {
        slides.slideTo(i)
        break;
      }
    }
  }

  async jumpTo(index: number) {
    this.slides.slideTo(index)
  }

  async protocollCompleted(slides) {
    this.slides.length().then(data => {
      this.slidesLength = data
    })
    await slides.getActiveIndex().then(index => {
      this.currentSlide = index + 1;

      if (index === 0) {
        slides.length().then(data => {
          if (this.entry != undefined) {
            if (this.entry[data - 1] == "completed") {
              slides.slideTo(data)
            }
          }
        })
      }
    });
  }

  async prev(slides) {
    slides.slidePrev(); // slide to next
    this.slides.length().then(data => {

      this.slides.getActiveIndex().then(res => {
        if (res != data) {
          this.isLastPage = false
        }
        if (res == data) {
          this.isLastPage = true
        }
        if (res != 1) {
          this.isFirstPage = false
        }
        if (res == 1) {
          this.isFirstPage = true
        }
      })
    })

    await slides.getActiveIndex().then(index => {
      this.currentSlide = index + 1;
    })
  }

  async next(slides) {
    slides.slideNext(); // slide to next
    this.slides.length().then(data => {
      this.slides.getActiveIndex().then(res => {
        if (res != data - 1) {
          this.isLastPage = false
        }
        if (res == data - 1) {
          this.isLastPage = true
        }
        if (res != 0) {
          this.isFirstPage = false
        }
        if (res == 0) {
          this.isFirstPage = true
        }
      })
    })

    await slides.getActiveIndex().then(index => {
      this.currentSlide = index + 1;
    })
  }

}
