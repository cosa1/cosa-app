import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { ToastController, ModalController } from '@ionic/angular';
import { OfflineAlertService } from '@app/applicationlogic/alert/offline-alert.service';


@Component({
  selector: 'app-titlebar',
  templateUrl: './titlebar.component.html',
  styleUrls: ['./titlebar.component.scss'],
})
export class TitlebarComponent implements OnInit {
  @Input() titletext: string;
  @Input() showMenuButton = false;
  @Input() showBackButton = true;
  @Input() showAlertButton = false;
  @Input() showCloseButton = false;
  @Input() showManuallyButton = false;
  @Input() setHref: string;
  @Input() showMyAlert = false;

  constructor(
    public toastController: ToastController,
    private modalController: ModalController,
  ) {
  }

  ngOnInit() {
  }

  async openToast() {
    if (this.showMyAlert == true) {
      const toast = await this.toastController.create({
        message: 'Ihre letzten Aktionen wurden nicht in der Datenbank gespeichert, damit diese nicht verloren gehen, stellen Sie eine Internetverbindung her und synchronisieren Sie unter dem Reiter "Mein Account".',
        position: 'bottom',
        buttons: [
          {
            text: 'x',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      toast.present();
      toast.onDidDismiss().then((val) => {
        console.log('toast dismissed');
      });
    }
  }

  dismissSelf(): void {
    this.modalController.dismiss();
  }

}
