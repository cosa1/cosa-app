import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../applicationlogic/auth/services/authentication.service';
import { ModalController, Platform } from '@ionic/angular';
import { User } from '../data/models/user';
import { ImprintPage } from './pages/imprint/imprint.page';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public navPages = [
    { title: 'Home', url: '/home', icon: 'home' },
    { title: 'Training wählen', url: '/select-training', icon: 'bulb' },
    //{ title: 'Schlafeffizienz', url: '/sleep-efficiency', icon: 'bed' },
    { title: 'Mein Account', url: '/my-account', icon: 'person-circle' },
  ];
  currentUser: User;
  logo = "assets/geton-logo.png";

  constructor(
    private platform: Platform,
    private auth: AuthenticationService,
    private modalController: ModalController,
  ) {
    this.initializeApp();
  }

  ngOnInit(): void {
    const path = window.location.pathname.split('/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.navPages.findIndex(page => page.url.toLowerCase() === '/' + path.toLowerCase());
    }
    if(environment.production == true && environment.project == "c4c"){
      this.logo = "assets/c4c-logo.png";
    }
  }

  private initializeApp() {
    this.platform.ready().then(() => {
      this.auth.getCurrentUser().subscribe(user => {
        this.currentUser = user;
      });
    });
  }

  async openImprintModal() {
    const modal = await this.modalController.create({
      component: ImprintPage,
    });
    return await modal.present();
  }
}
