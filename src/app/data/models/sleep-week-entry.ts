export interface SleepWeekEntry {
    id?: number;
    title: String;
    date: Date;
    days?: any[];
    text?: string;
    status?: string; // Todo: Make enum
    efficiency?: number;
  }