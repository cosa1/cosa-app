export interface SleepEntry {
    id?: number;
    date: Date;
    toBed: any;
    asleep: any;
    wokeUp: any;
    getUp: any;
    text?: string;
    status?: string; // Todo: Make enum
    efficiency: number;
    weekId?: number;
    shift: string;
    app_sleep_data_questions?: any
    sleepValue?: number;

  }