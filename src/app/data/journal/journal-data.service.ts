import { Injectable } from '@angular/core';
import { environment } from '@environment/environment';
import { JournalEntry } from '@app/data/models/journal-entry';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JournalDataService {

  private urlUpdateJournalEntries = environment.api_base_url + '/v1/journal';
  private urlGetAllJournals = environment.api_base_url + '/v1/get-all-entries';
  private urlAddJournalEntry = environment.api_base_url + '/v1/add-journal-entry';
  private urlDelJournalEntry = environment.api_base_url + '/v1/del-journal-entry';

  constructor(private http: HttpClient) {
  }

  public async updateJournalEntries(journalEntries: JournalEntry[]): Promise<JournalEntry[]> {
    return await this.http.post<JournalEntry[]>(this.urlUpdateJournalEntries, journalEntries).toPromise();
  }

  public async getAllJournals(): Promise<JournalEntry[]> { //Not in use at the moment
    return await this.http.get<JournalEntry[]>(this.urlGetAllJournals).toPromise();
  }

  public async addJournalEntry(journalEntry: JournalEntry): Promise<JournalEntry> {
    return await this.http.post<JournalEntry>(this.urlAddJournalEntry, journalEntry).toPromise();
  }

  public async delJournalEntry(journalEntryId: number): Promise<JournalEntry> {
    let params = new HttpParams();
    params = params.append('_delEntry', journalEntryId.toString())
    return await this.http.delete<JournalEntry>(this.urlDelJournalEntry, { params: params }).toPromise();
  }
}