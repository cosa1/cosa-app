import { TestBed } from '@angular/core/testing';
import { SleepEfficiencyService } from './sleep-efficiency.service';

describe('SleepEfficiencyService', () => {
  let service: SleepEfficiencyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SleepEfficiencyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});