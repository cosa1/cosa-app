import { TestBed } from '@angular/core/testing';
import { SleepEfficiencyStorageService } from './sleep-efficiency-storage.service';

describe('SleepEfficiencyStorageService', () => {
  let service: SleepEfficiencyStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SleepEfficiencyStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});