import { Injectable } from '@angular/core';
import { environment } from '@environment/environment';
import { SleepEntry } from '@app/data/models/sleep-entry';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SleepWeekEntry } from '@app/data/models/sleep-week-entry';

@Injectable({
  providedIn: 'root'
})
export class SleepEfficiencyService {

  private urlUpdateSleepEntries = environment.api_base_url + '/v1/sleep';
  private urlAddSleepEntry = environment.api_base_url + '/v1/add-sleep-entry';
  private urlDelSleepEntry = environment.api_base_url + '/v1/del-sleep-entry';
  private urlAddSleepWeekEntry = environment.api_base_url + '/v1/add-sleep-week-entry';
  private urlUpdateSleepWeeks = environment.api_base_url + '/v1/sleep-weeks';
  private urlUpdateSleepWeekEntry = environment.api_base_url + '/v1/update-sleep-week-entry';

  constructor(private http: HttpClient) {
  }

  public async updateSleepEntries(SleepWeeks: SleepEntry[]): Promise<SleepEntry[]> {
    return await this.http.post<SleepEntry[]>(this.urlUpdateSleepEntries, SleepWeeks).toPromise();
  }

  public async updateSleepWeeks(SleepEntries: SleepWeekEntry[]): Promise<SleepWeekEntry[]> {
    return await this.http.post<SleepWeekEntry[]>(this.urlUpdateSleepWeeks, SleepEntries).toPromise();
  }

  public async addSleepEntry(SleepEntry: SleepEntry): Promise<SleepEntry> {
    return await this.http.post<SleepEntry>(this.urlAddSleepEntry, SleepEntry).toPromise();
  }

  public async delSleepEntry(sleepEntryId: number): Promise<SleepEntry> {
    let params = new HttpParams();
    params = params.append('_delSleepEntry', sleepEntryId.toString())
    return await this.http.delete<SleepEntry>(this.urlDelSleepEntry, { params: params }).toPromise();
  }

  public async delSleepWeek(sleepEntryId: number): Promise<SleepWeekEntry> {
    let params = new HttpParams();
    params = params.append('_delSleepEntry', sleepEntryId.toString())
    return await this.http.delete<SleepWeekEntry>(this.urlDelSleepEntry, { params: params }).toPromise();
  }

  public async addSleepWeek(SleepWeekEntry: SleepWeekEntry): Promise<SleepWeekEntry> {
    return await this.http.post<SleepWeekEntry>(this.urlAddSleepWeekEntry, SleepWeekEntry).toPromise();
  }

  public async updateSleepWeek(week: any): Promise<any> {
    return await this.http.post<any>(this.urlUpdateSleepWeekEntry, week).toPromise();
  }
}