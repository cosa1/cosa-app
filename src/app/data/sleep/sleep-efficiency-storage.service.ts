import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/core';
import { SleepEntry } from '@app/data/models/sleep-entry';
import { SleepWeekEntry } from '@app/data/models/sleep-week-entry';

@Injectable({
  providedIn: 'root'
})
export class SleepEfficiencyStorageService {
  private readonly STORAGE_KEY = 'sleep_entries';
  private readonly STORAGE_KEY_SLEEP_WEEK = 'sleep_week_entries';
  constructor() { }

  async loadEntriesFromStorage(): Promise<SleepEntry[]> {
    const entries = JSON.parse((await Storage.get({ key: this.STORAGE_KEY })).value) ?? [];
    entries.forEach((item: SleepEntry, index: number, arr: SleepEntry[]) => {
      arr[index].date = new Date(item.date);
    });
    return entries;
  }

  async loadWeeksFromStorage(): Promise<SleepWeekEntry[]> {
    const entries = JSON.parse((await Storage.get({ key: this.STORAGE_KEY_SLEEP_WEEK })).value) ?? [];
    entries.forEach((item: SleepWeekEntry, index: number, arr: SleepWeekEntry[]) => {
      arr[index].date = new Date(item.date);
    });
    return entries;
  }

  async saveEntriesToStorage(entries: SleepEntry[]): Promise<void> {
    return Storage.set({
      key: this.STORAGE_KEY,
      value: JSON.stringify(entries)
    });
  }

  async saveWeeksToStorage(entries: SleepWeekEntry[]): Promise<void> {
    return Storage.set({
      key: this.STORAGE_KEY_SLEEP_WEEK,
      value: JSON.stringify(entries)
    });
  }
}