import { Injectable } from '@angular/core';
import { JournalStorageService } from '@app/data/journal/journal-storage.service';
import { JournalEntry } from '@app/data/models/journal-entry';
import { JournalDataService } from '@app/data/journal/journal-data.service';
import { Storage } from '@capacitor/core';


@Injectable({
  providedIn: 'root'
})
export class JournalService {

  constructor(private journalStorageService: JournalStorageService, private journalDataService: JournalDataService) {
  }

  public async getAllJournalEntriesForTrainingId(trainingId: number): Promise<JournalEntry[]> {
    const entries = await this.journalStorageService.loadEntriesFromStorage();
    return entries.filter(e => e.status !== 'deleted');
  }

  public async addNewEntry(newEntry: JournalEntry): Promise<void> {
    const entries = await this.journalStorageService.loadEntriesFromStorage();
    entries.unshift(newEntry);
    await this.journalStorageService.saveEntriesToStorage(entries);
  }

  public async deleteEntry(entry: JournalEntry) {
    const entries = await this.journalStorageService.loadEntriesFromStorage();
    const i = entries.findIndex(e => e.date.valueOf() === entry.date.valueOf());

    await this.journalDataService.delJournalEntry(entry.id).then(event => {
      entries.splice(i, 1);
    }).catch(error => {
      entries[i].status = 'deleted';
      Storage.set({
        key: 'alert',
        value: JSON.stringify(true)
      });
    });

    await this.journalStorageService.saveEntriesToStorage(entries);
  }
}
