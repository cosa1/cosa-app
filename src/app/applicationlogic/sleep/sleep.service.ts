import { Injectable } from '@angular/core';
import { SleepEfficiencyStorageService } from '@app/data/sleep/sleep-efficiency-storage.service';
import { SleepEntry } from '@app/data/models/sleep-entry';

import { SleepEfficiencyService } from '@app/data/sleep/sleep-efficiency.service';
import { Storage } from '@capacitor/core';

import { SleepWeekEntry } from '@app/data/models/sleep-week-entry';

@Injectable({
  providedIn: 'root'
})
export class SleepService {

  constructor(private sleepEfficiencyStorageService: SleepEfficiencyStorageService, private sleepEfficiencyService: SleepEfficiencyService) {
  }

  public async getAllEntriesWeekId(): Promise<SleepEntry[]> {
    const entries = await this.sleepEfficiencyStorageService.loadEntriesFromStorage();
    return entries.filter(e => e.status !== 'deleted');
  }

  public async getAllWeeks(): Promise<SleepWeekEntry[]> {
    const entries = await this.sleepEfficiencyStorageService.loadWeeksFromStorage();
    return entries.filter(e => e.status !== 'deleted');
  }

  public async addNewEntry(newEntry: SleepEntry): Promise<void> {
    const entries = await this.sleepEfficiencyStorageService.loadEntriesFromStorage();
    entries.unshift(newEntry);
    await this.sleepEfficiencyStorageService.saveEntriesToStorage(entries);
  }

  public async addNewWeekEntry(newEntry: SleepWeekEntry): Promise<void> {
    const entries = await this.sleepEfficiencyStorageService.loadWeeksFromStorage();
    entries.unshift(newEntry);
    await this.sleepEfficiencyStorageService.saveWeeksToStorage(entries);
  }

  public async deleteEntry(entry: SleepEntry, indexLocal: number) {
    const entries = await this.sleepEfficiencyStorageService.loadEntriesFromStorage();

    await this.sleepEfficiencyService.delSleepEntry(entry.id).then(event => {
      entries.splice(indexLocal, 1);
    }).catch(error => {
      entries[indexLocal].status = 'deleted';
      Storage.set({
        key: 'alert',
        value: JSON.stringify(true)
      });
    });

    await this.sleepEfficiencyStorageService.saveEntriesToStorage(entries);
  }

  public async deleteWeek(entry: SleepWeekEntry, indexLocal: number) {
    const entries = await this.sleepEfficiencyStorageService.loadWeeksFromStorage();
    await this.sleepEfficiencyService.delSleepWeek(entry.id).then(event => {
      entries.splice(indexLocal, 1);
    }).catch(error => {
      entries[indexLocal].status = 'deleted';
      Storage.set({
        key: 'alert',
        value: JSON.stringify(true)
      });
    });

    await this.sleepEfficiencyStorageService.saveWeeksToStorage(entries);
  }

  public async addWeek(sleepWeeks: SleepWeekEntry) {
    const entries = await this.sleepEfficiencyStorageService.loadWeeksFromStorage();

    await this.sleepEfficiencyService.addSleepWeek(sleepWeeks).then(event => {
      let newWeekEntryId = JSON.stringify(event);
      // Add ID to the Object which is returned from the server
      Object.assign(sleepWeeks, { id: newWeekEntryId });

      this.addNewWeekEntry(sleepWeeks); // Save new journal Entry in Local Storage
      entries.unshift(sleepWeeks);
      this.sleepEfficiencyStorageService.saveWeeksToStorage(entries);

    }).catch(error => {
      Object.assign(sleepWeeks, { status: "added" });
      this.addNewWeekEntry(sleepWeeks); // Save new journal Entry in Local Storage with flag if error
      entries.unshift(sleepWeeks);
      Storage.set({
        key: 'alert',
        value: JSON.stringify(true)
      });
      this.sleepEfficiencyStorageService.saveWeeksToStorage(entries);
    });
  }

  async updateWeek(sleepWeek, allWeeks, urlWeekId) {
    //UPDATE week
    this.sleepEfficiencyService.updateSleepWeek(allWeeks[urlWeekId]).then(() => {
      this.sleepEfficiencyStorageService.saveWeeksToStorage(allWeeks);
    }).catch(error => {
      allWeeks[urlWeekId].status = 'updated';
      Storage.set({
        key: 'alert',
        value: JSON.stringify(true)
      }).then(() => {
        this.sleepEfficiencyStorageService.saveWeeksToStorage(allWeeks);
      });
    })
  }
}