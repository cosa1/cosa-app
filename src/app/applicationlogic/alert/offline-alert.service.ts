import { Injectable, Input } from '@angular/core';
import { Storage } from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class OfflineAlertService {

  @Input() showMyAlert = false;

  constructor() { }

  async loadingAlert() {
    const data = await Storage.get({ key: 'alert' });
    this.showMyAlert = JSON.parse(data.value) ?? false;
    return this.showMyAlert;
  }
}
