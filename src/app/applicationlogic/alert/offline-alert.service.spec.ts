import { TestBed } from '@angular/core/testing';

import { OfflineAlertService } from './offline-alert.service';

describe('OfflineAlertService', () => {
  let service: OfflineAlertService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfflineAlertService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
